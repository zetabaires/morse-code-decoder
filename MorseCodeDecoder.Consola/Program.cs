﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MorseCodeDecoder.Common.Contracts;
using MorseCodeDecoder.Common.Encoder;

namespace MorseCodeDecoder.Consola
{
    /// <summary>
    /// Programa prinicipal.
    /// </summary>
    public class Program
    {
        private readonly ServiceProvider serviceProvider;
        private readonly ILogger<Program> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="Program"/> class.
        /// </summary>
        public Program()
        {
            // inicializa una instancia de ServiceCollection para poder inicializar los objetos como si fuera un proyecto web
            // Y así compartir las librerías
            var serviceDescriptors = new ServiceCollection();

            // Agrega las dependencias necesarias
            serviceDescriptors.AddLogging(m => m.AddConsole());
            serviceDescriptors.AddTransient<BinaryMessageDecoder>();
            serviceDescriptors.AddTransient<MorseMessageManager>();

            // Compila para obtener el service provider o poder pedir servicios
            this.serviceProvider = serviceDescriptors.BuildServiceProvider();

            // Obtiene una instancia del logger
            this.logger = this.serviceProvider.GetService<ILogger<Program>>();
        }

        /// <summary>
        /// Método Main que inicializa el proyecto.
        /// </summary>
        /// <param name="args">Argumentos de la aplicación.</param>
        public static void Main(string[] args)
        {
            new Program().ShowMenu().ConfigureAwait(true);
        }

        /// <summary>
        /// Método de clase para mostrar el menu de opciones.
        /// </summary>
        /// <returns>Task</returns>
        private async Task ShowMenu()
        {
            const int SALIR = 9;

            int opt;
            do
            {
                // Muestra las opciones
                Console.WriteLine("Selecciona una opción: ");
                Console.WriteLine("01 - Decodificar Bits a Morse.");
                Console.WriteLine("02 - Decodificar Morse a Humano.");
                Console.WriteLine("03 - Codificar de Humano a Morse."); // Feature parte web
                Console.WriteLine($"0{SALIR} - Salir");

                // Valida la opción ingresada por el usuario, si no es un número tira error y vuelve a intentar
                if (!int.TryParse(Console.ReadLine(), out opt))
                {
                    Console.Error.WriteLine("No es un número!");
                    continue;
                }

                switch (opt)
                {
                    case 1:
                        await this.DecodeBits2Morse();
                        break;
                    case 2:
                        await this.Translate2Human();
                        break;
                    case 3:
                        await this.EncodeHuman2Morse();
                        break;
                    case SALIR:
                        Console.WriteLine("Gracias por utilizar la app (:");
                        break;
                    default:
                        // Si no es una opción válida, tira error y vuelve a intentar
                        Console.Error.WriteLine("Opción invalida!");
                        break;
                }
            }
            while (opt != SALIR);
        }

        /// <summary>
        /// Método que decodifica una secuencia binaria a código MORSE.
        /// </summary>
        private Task DecodeBits2Morse()
        {
            return this.DecodeProcess<BinaryMessageDecoder>("Ingresa la secuencia binaria: ");
        }

        /// <summary>
        /// Método que decodifica una secuencia en morse a lenguaje humano.
        /// </summary>
        private Task Translate2Human()
        {
            return this.DecodeProcess<MorseMessageManager>("Ingresa la secuencia en morse: ");
        }

        /// <summary>
        /// Método que codifica texto en lenguaje humano a código MORSE.
        /// </summary>
        private Task EncodeHuman2Morse()
        {
            return this.EncodeProcess<MorseMessageManager>("Ingresa el texto: ");
        }

        #region [Internal]

        /// <summary>
        /// Método genérico para pedir los datos y empezar a decodificar.
        /// </summary>
        /// <typeparam name="TDecoder">Tipo de decodificador a instanciar.</typeparam>
        /// <param name="messageInput">Mensaje a mostrar en pantalla.</param>
        private Task DecodeProcess<TDecoder>(string messageInput)
            where TDecoder : IMessageDecoder
        {
            return this.Process<TDecoder>(messageInput, (decoder, input) => decoder.DecodeMessage(input));
        }

        /// <summary>
        /// Método genérico para pedir los datos y empezar a codificar.
        /// </summary>
        /// <typeparam name="TEncoder">Tipo de codificador a instanciar.</typeparam>
        /// <param name="messageInput">Mensaje a mostrar en pantalla.</param>
        private Task EncodeProcess<TEncoder>(string messageInput)
            where TEncoder : IMessageEncoder
        {
            return this.Process<TEncoder>(messageInput, (encoder, input) => encoder.EncodeMessage(input));
        }

        /// <summary>
        /// Método genérico para pedir los datos y empezar a manipular los mensajes.
        /// </summary>
        /// <typeparam name="TManager">Tipo de codificador o decodificador a instanciar.</typeparam>
        /// <param name="messageInput">Mensaje a mostrar en pantalla.</param>
        /// <param name="action">Acción a realizar (codificar o decodificar).</param>
        private async Task Process<TManager>(string messageInput, Func<TManager, string, Task<string>> action)
        {
            // Imprime en pantalla un mensaje para indicarle al usuario que ingrese el input
            Console.WriteLine(messageInput);

            // Lee el ingreso de datos del usuario
            string input = Console.ReadLine();

            try
            {
                // Obtiene una instancia de un manager (decoder o encoder)
                var manager = this.serviceProvider.GetService<TManager>();

                // Invoca la función correspondiente (decode o encode)
                var result = await action.Invoke(manager, input);

                // Muesta al usuario el resultado
                Console.WriteLine("El resultado es: " + result);
            }
            catch (Exception ex)
            {
                string errorMessage;
                this.logger.LogError(ex, errorMessage = "Ocurrio un error!");
                Console.Error.WriteLine(errorMessage);
            }
        }

        #endregion
    }
}
