# README #

Este README explica brevemente los distintos proyectos alojados en este repositorio.

### Aplicaciones en este repositorio? ###

* MorseCodeDecoder.Consola **(console)** (Principal)
* MorseCodeDecoder **(webapi)**

### Además? ###

* MorseCodeDecoder.Common **(librería de clases)**
* MorseCodeDecoder.Tests **(proyecto de tests unitarios)**

### Breve explicación ###

* Se crearon 2 proyectos. una app de consola (app principal del ejercicio) y otra web (bonus) que comparten las mismas librerías para codificar y descodificar los mensajes en sus distintas formas (binario, morse, etc), junto con un proyecto de Test con todos los unittest de cada componente.

### Comentarios ###

* Para el caso del proyecto web se creo una capa de Business con la intención de alojar allí todo lo que se refiera a lógica de negocio. La idea de no tenerlo acoplado en el controller es sinplemente para separar los contratos de los servicios de la lógica misma. Si bien ahora solo implementa las librerías en común, la idea es que todas las interacciones sean alojadas allí, en caso de que la aplicación creciera.