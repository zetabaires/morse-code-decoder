﻿namespace MorseCodeDecoder.Tests
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using MorseCodeDecoder.Common.Encoder;

    /// <summary>
    /// Clase de Tests para la clase BinaryMessageDecoder.
    /// </summary>
    [TestClass]
    public class BinaryMessageDecoderTest
    {
        private const string HolaMeliBinary = "000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000";
        private const string HolaMeliMorse = ".... --- .-.. .- -- . .-.. ..";

        private BinaryMessageDecoder binaryMessageDecoder;

        /// <summary>
        /// Método que inicializa las variables de clase.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            var logger = new Mock<ILogger<BinaryMessageDecoder>>().Object;

            this.binaryMessageDecoder = new BinaryMessageDecoder(logger);
        }

        /// <summary>
        /// Método que checkea el funcionamiento correcto de la decodificación de Binario a Morse.
        /// </summary>
        [TestMethod]
        public async Task DecodeMessageOk()
        {
            var result = await this.binaryMessageDecoder.DecodeMessage(HolaMeliBinary);

            Assert.AreEqual(HolaMeliMorse, result);
        }

        /// <summary>
        /// Método que checkea el funcionamiento erroneo de la decodificación de Binario a Morse.
        /// </summary>
        [TestMethod]
        public async Task DecodeMessageError()
        {
            var result = await this.binaryMessageDecoder.DecodeMessage("1100" + HolaMeliBinary);

            Assert.AreNotEqual(HolaMeliMorse, result);
        }

        /// <summary>
        /// Método que checkea el que la decodificación lance excepción cuando recibe un argumento inválido.
        /// </summary>
        [TestMethod]
        public Task DecodeMessageErrorException()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.binaryMessageDecoder.DecodeMessage("000111.-");
            });
        }

        /// <summary>
        /// Método que checkea el que la decodificación lance excepción cuando recibe un argumento null.
        /// </summary>
        [TestMethod]
        public Task DecodeMessageErrorExceptionNull()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.binaryMessageDecoder.DecodeMessage(null);
            });
        }

        /// <summary>
        /// Método que checkea el que la decodificación lance excepción cuando recibe un argumento vacio.
        /// </summary>
        [TestMethod]
        public Task DecodeMessageErrorExceptionEmpty()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.binaryMessageDecoder.DecodeMessage(string.Empty);
            });
        }
    }
}
