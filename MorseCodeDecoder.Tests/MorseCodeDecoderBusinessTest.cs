namespace MorseCodeDecoder.Tests
{
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using MorseCodeDecoder.Business;
    using MorseCodeDecoder.Common.Contracts;

    /// <summary>
    /// Clase de Tests para la clase MorseCodeDecoderBusiness.
    /// </summary>
    [TestClass]
    public class MorseCodeDecoderBusinessTest
    {
        private Mock<IMessageDecoder> decoderMock;
        private Mock<IMessageEncoder> encoderMock;
        private MorseCodeDecoderBusiness business;

        /// <summary>
        /// M�todo que inicializa las variables de clase.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            var loggerBusiness = new Mock<ILogger<MorseCodeDecoderBusiness>>().Object;

            this.decoderMock = new Mock<IMessageDecoder>();
            this.encoderMock = new Mock<IMessageEncoder>();

            this.business = new MorseCodeDecoderBusiness(loggerBusiness, this.decoderMock.Object, this.encoderMock.Object);
        }

        /// <summary>
        /// M�todo que valida que el business llame a la funci�n correcta de su dependencia al momento de decodificar de Morse a Humano.
        /// </summary>
        [TestMethod]
        public async Task TestTranslate2HumanOk()
        {
            string message = ".... --- .-.. .- -- . .-.. ..";
            var result = await this.business.Translate2Human(message);

            // Valida que se llame una vez a la funci�n DecodeMessage del decoder con el mensaje que recibi� el business.
            this.decoderMock.Verify(m => m.DecodeMessage(message), Times.Once);
        }

        /// <summary>
        /// M�todo que valida que el business llame a la funci�n correcta de su dependencia al momento de codificar de Humano a Morse.
        /// </summary>
        [TestMethod]
        public async Task TestTranslate2MorseOk()
        {
            string message = "HOLA MELI";
            var result = await this.business.Translate2Morse(message);

            // Valida que se llame una vez a la funci�n EncodeMessage del encoder con el mensaje que recibi� el business.
            this.encoderMock.Verify(m => m.EncodeMessage(message), Times.Once);
        }
    }
}
