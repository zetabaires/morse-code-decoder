﻿namespace MorseCodeDecoder.Tests
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using MorseCodeDecoder.Common.Encoder;

    /// <summary>
    /// Clase de Tests para la clase MorseMessageManager.
    /// </summary>
    [TestClass]
    public class MorseMessageManagerTest
    {
        private const string HolaMeliMorse = ".... --- .-.. .- -- . .-.. ..";
        private const string HolaMeliHuman = "HOLAMELI";

        private MorseMessageManager morseMessageManager;

        /// <summary>
        /// Método que inicializa las variables de clase.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            var loggerMessageManager = new Mock<ILogger<MorseMessageManager>>().Object;

            this.morseMessageManager = new MorseMessageManager(loggerMessageManager);
        }

        /// <summary>
        /// Método que checkea el funcionamiento correcto de la decodificación de Morse a Humano.
        /// </summary>
        [TestMethod]
        public async Task DecodeMessageOk()
        {
            var result = await this.morseMessageManager.DecodeMessage(HolaMeliMorse);

            Assert.AreEqual(HolaMeliHuman, result);
        }

        /// <summary>
        /// Método que checkea el funcionamiento erroneo de la decodificación de Morse a Humano.
        /// </summary>
        [TestMethod]
        public async Task DecodeMessageError()
        {
            var result = await this.morseMessageManager.DecodeMessage("-- " + HolaMeliMorse);

            Assert.AreNotEqual(HolaMeliHuman, result);
        }

        /// <summary>
        /// Método que checkea el que la decodificación lance excepción cuando recibe un argumento inválido.
        /// </summary>
        [TestMethod]
        public Task DecodeMessageErrorException()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.morseMessageManager.DecodeMessage("....asdsa");
            });
        }

        /// <summary>
        /// Método que checkea el que la decodificación lance excepción cuando recibe un argumento null.
        /// </summary>
        [TestMethod]
        public Task DecodeMessageErrorExceptionNull()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.morseMessageManager.DecodeMessage(null);
            });
        }

        /// <summary>
        /// Método que checkea el que la decodificación lance excepción cuando recibe un argumento vacio.
        /// </summary>
        [TestMethod]
        public Task DecodeMessageErrorExceptionEmpty()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.morseMessageManager.DecodeMessage(string.Empty);
            });
        }

        /// <summary>
        /// Método que checkea el funcionamiento correcto de la codificación de Humano a Morse.
        /// </summary>
        [TestMethod]
        public async Task EncodeMessageOk()
        {
            var result = await this.morseMessageManager.EncodeMessage(HolaMeliHuman);

            Assert.AreEqual(HolaMeliMorse, result);
        }

        /// <summary>
        /// Método que checkea el funcionamiento erroneo de la codificación de Humano a Morse.
        /// </summary>
        [TestMethod]
        public async Task EncodeMessageError()
        {
            var result = await this.morseMessageManager.EncodeMessage("asd " + HolaMeliHuman);

            Assert.AreNotEqual(HolaMeliMorse, result);
        }

        /// <summary>
        /// Método que checkea el que la codificación lance excepción cuando recibe un argumento con catacteres especiales.
        /// </summary>
        [TestMethod]
        public Task EncodeMessageErrorException()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.morseMessageManager.EncodeMessage("asd??%&");
            });
        }

        /// <summary>
        /// Método que checkea el que la codificación lance excepción cuando recibe un argumento null.
        /// </summary>
        [TestMethod]
        public Task EncodeMessageErrorExceptionNull()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.morseMessageManager.EncodeMessage(null);
            });
        }

        /// <summary>
        /// Método que checkea el que la codificación lance excepción cuando recibe un argumento vacio.
        /// </summary>
        [TestMethod]
        public Task EncodeMessageErrorExceptionEmpty()
        {
            return Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                var result = await this.morseMessageManager.EncodeMessage(string.Empty);
            });
        }
    }
}
