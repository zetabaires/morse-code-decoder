﻿using System.ComponentModel.DataAnnotations;

namespace MorseCodeDecoder.Domain
{
    /// <summary>
    /// Objeto genérico para respuestas de servicios.
    /// </summary>
    public class ResponseDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseDto"/> class.
        /// </summary>
        /// <param name="message">Valor de la respuesta</param>
        public ResponseDto(string message) => this.Message = message;

        /// <summary>
        /// Gets or sets valor de la respuesta.
        /// </summary>
        [Display(Name = "Mensaje")]
        public string Message { get; set; }
    }
}
