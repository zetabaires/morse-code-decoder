﻿using System.ComponentModel.DataAnnotations;

namespace MorseCodeDecoder.Domain
{
    /// <summary>
    /// Objeto genérico para peticiones de servicios.
    /// </summary>
    public class RequestDto
    {
        /// <summary>
        /// Gets or sets valor de la petición.
        /// </summary>
        [Display(Name = "Texto")]
        [Required(ErrorMessage = "El {0} es requerido")]
        [MinLength(1, ErrorMessage = "El largo del campo {0} debe ser minimamente mayor a {1}")]
        public string Text { get; set; }
    }
}
