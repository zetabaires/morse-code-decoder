﻿namespace MorseCodeDecoder.Domain.Attributes
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text.RegularExpressions;
    using MorseCodeDecoder.Common;

    /// <summary>
    /// Atributo designado a validar que la propiedad Text dentro de un objeto de tipo RequestDto tenga el formato adecuado.
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
    public class MorseCodeTextAttribute : ValidationAttribute
    {
        private readonly bool isMorseCodeFormat;

        /// <summary>
        /// Initializes a new instance of the <see cref="MorseCodeTextAttribute"/> class.
        /// </summary>
        /// <param name="isMorseCodeFormat">Flag para indicar si el texto debe venir en formato morse.</param>
        public MorseCodeTextAttribute(bool isMorseCodeFormat)
        {
            this.isMorseCodeFormat = isMorseCodeFormat;
        }

        /// <summary>
        /// Método que valida el formato de entrada del texto en un request.
        /// </summary>
        /// <param name="value">Valor del objeto al momento de validar.</param>
        /// <returns>Estado de la validación.</returns>
        public override bool IsValid(object value)
        {
            var request = value as RequestDto;

            // Si el objeto es null es porque no se pudo aplicar el casteo
            if (request == null)
            {
                throw new Exception("Mala implementación del atributo");
            }

            // Si el campo está vacio no hace falta validar
            if (string.IsNullOrEmpty(request.Text))
            {
                return true;
            }

            if (this.isMorseCodeFormat)
            {
                char[] available = new char[] { Constants.DOT, Constants.DASH, Constants.SPACE };
                return !request.Text.Any(m => !available.Contains(m));
            }
            else
            {
                return Regex.IsMatch(request.Text.ToUpperInvariant(), "^[A-Z0-9 ]*$");
            }
        }

        /// <summary>
        /// Formateador de mensaje de error.
        /// </summary>
        /// <param name="name">Nombre de la propiedad en cuestion.</param>
        /// <returns>Mensaje de error formateado.</returns>
        public override string FormatErrorMessage(string name)
        {
            if (this.isMorseCodeFormat)
            {
                return string.Format(this.ErrorMessage ?? "El objeto {0} no tiene formato de código morse", name);
            }
            else
            {
                return string.Format(this.ErrorMessage ?? "El objeto {0} no puede contener caracteres especiales", name);
            }
        }
    }
}
