﻿namespace MorseCodeDecoder.Services
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using MorseCodeDecoder.Business.Contracts;
    using MorseCodeDecoder.Domain;
    using MorseCodeDecoder.Domain.Attributes;

    /// <summary>
    /// Controlador designado para exponer las URLs necesarias para este examen.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class MorseCodeController : ControllerBase
    {
        private readonly ILogger<MorseCodeController> logger;
        private readonly IMorseCodeDecoderBusiness morseCodeDecoderBusiness;

        /// <summary>
        /// Initializes a new instance of the <see cref="MorseCodeController"/> class.
        /// </summary>
        /// <param name="logger">Instancia de logger para este controlador.</param>
        /// /// <param name="morseCodeDecoderBusiness">Instancia de business para las conversiones necesarias.</param>
        public MorseCodeController(ILogger<MorseCodeController> logger, IMorseCodeDecoderBusiness morseCodeDecoderBusiness)
        {
            this.logger = logger;
            this.morseCodeDecoderBusiness = morseCodeDecoderBusiness;
        }

        /// <summary>
        /// Endpoint que expone la funcionalidad que permite traducir texto de MORSE a lenguaje humano.
        /// </summary>
        /// <param name="request">Objecto request con el texto a traducir.</param>
        /// <returns>Mensaje en lenguaje humano.</returns>
        [HttpPost]
        [Route("translate/2text")]
        [ProducesResponseType(400, Type = typeof(ResponseDto))]
        [ProducesResponseType(200, Type = typeof(ResponseDto))]
        public async Task<IActionResult> Translate2Text([FromBody][MorseCodeText(true)] RequestDto request)
        {
            try
            {
                var message = await this.morseCodeDecoderBusiness.Translate2Human(request.Text);

                return this.Ok(new ResponseDto(message));
            }
            catch (Exception ex)
            {
                string errorMessage;
                this.logger.LogError(ex, errorMessage = "Ocurrio un error!");
                return this.BadRequest(new ResponseDto(errorMessage));
            }
        }

        /// <summary>
        /// Endpoint que expone la funcionalidad que permite traducir texto de lenguaje humano a MORSE.
        /// </summary>
        /// <param name="request">Objecto request con el texto a traducir.</param>
        /// <returns>Mensaje en lenguaje MORSE.</returns>
        [HttpPost]
        [Route("translate/2morse")]
        [ProducesResponseType(400, Type = typeof(ResponseDto))]
        [ProducesResponseType(200, Type = typeof(ResponseDto))]
        public async Task<IActionResult> TranslateToMorse([FromBody][MorseCodeText(false)] RequestDto request)
        {
            try
            {
                var message = await this.morseCodeDecoderBusiness.Translate2Morse(request.Text);

                return this.Ok(new ResponseDto(message));
            }
            catch (Exception ex)
            {
                string errorMessage;
                this.logger.LogError(ex, errorMessage = "Ocurrio un error!");
                return this.BadRequest(new ResponseDto(errorMessage));
            }
        }
    }
}
