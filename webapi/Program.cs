namespace MorseCodeDecoder
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;

    /// <summary>
    /// Programa prinicipal.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// M�todo Main que inicializa el proyecto web.
        /// </summary>
        /// <param name="args">Argumentos de la aplicaci�n.</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// M�todo que crea el WebHost a partir de la clase Startup.
        /// </summary>
        /// <param name="args">Argumentos de la aplicaci�n.</param>
        /// <returns>Instancia de HostBuilder.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
