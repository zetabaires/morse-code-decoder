﻿namespace MorseCodeDecoder.Business
{
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using MorseCodeDecoder.Business.Contracts;
    using MorseCodeDecoder.Common.Contracts;

    /// <summary>
    /// Clase con la lógica necesaria para codificar y decodificar los mensajes en MORSE.
    /// </summary>
    public class MorseCodeDecoderBusiness : IMorseCodeDecoderBusiness
    {
        private readonly ILogger<MorseCodeDecoderBusiness> logger;
        private readonly IMessageDecoder messageDecoder;
        private readonly IMessageEncoder messageEncoder;

        /// <summary>
        /// Initializes a new instance of the <see cref="MorseCodeDecoderBusiness"/> class.
        /// </summary>
        /// <param name="logger">Instancia de logger para esta clase.</param>
        /// <param name="messageDecoder">Instancia de objeto decodificador en MORSE.</param>
        /// <param name="messageEncoder">Instancia de objeto codificador a MORSE.</param>
        public MorseCodeDecoderBusiness(ILogger<MorseCodeDecoderBusiness> logger, IMessageDecoder messageDecoder, IMessageEncoder messageEncoder)
        {
            this.logger = logger;
            this.messageDecoder = messageDecoder;
            this.messageEncoder = messageEncoder;
        }

        /// <summary>
        /// Método que resuelve la conversión de texto en lenguaje humano a código MORSE.
        /// </summary>
        /// <param name="text">Código en lenguaje humano.</param>
        /// <returns>Texto en formato MORSE.</returns>
        public async Task<string> Translate2Morse(string text)
        {
            this.logger.LogInformation("Translate2Morse: " + text);

            this.logger.LogDebug("Por realizar el encoding del mensaje");
            string result = await this.messageEncoder.EncodeMessage(text);
            this.logger.LogDebug("Encoding del mensaje realizado");

            return result;
        }

        /// <summary>
        /// Método que resuelve la conversión de código MORSE a lenguaje humano.
        /// </summary>
        /// <param name="morseCode">Código en formato MORSE.</param>
        /// <returns>Texto en lenguaje humano.</returns>
        public async Task<string> Translate2Human(string morseCode)
        {
            this.logger.LogInformation("Translate2Human: " + morseCode);

            this.logger.LogDebug("Por realizar el decoding del mensaje");
            string result = await this.messageDecoder.DecodeMessage(morseCode);
            this.logger.LogDebug("Decoding del mensaje realizado");

            return result;
        }
    }
}
