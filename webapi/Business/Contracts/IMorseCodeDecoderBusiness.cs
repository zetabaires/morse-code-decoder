﻿namespace MorseCodeDecoder.Business.Contracts
{
    using System.Threading.Tasks;

    /// <summary>
    /// Interfaz que define el contrato necesario para codificar y decodificar los mensajes en MORSE.
    /// </summary>
    public interface IMorseCodeDecoderBusiness
    {
        /// <summary>
        /// Método que resuelve la conversión de texto en lenguaje humano a código MORSE.
        /// </summary>
        /// <param name="text">Código en lenguaje humano.</param>
        /// <returns>Texto en formato MORSE.</returns>
        Task<string> Translate2Morse(string text);

        /// <summary>
        /// Método que resuelve la conversión de código MORSE a lenguaje humano.
        /// </summary>
        /// <param name="morseCode">Código en formato MORSE.</param>
        /// <returns>Texto en lenguaje humano.</returns>
        Task<string> Translate2Human(string morseCode);
    }
}
