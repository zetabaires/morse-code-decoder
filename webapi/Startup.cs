namespace MorseCodeDecoder
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using MorseCodeDecoder.Business;
    using MorseCodeDecoder.Business.Contracts;
    using MorseCodeDecoder.Common.Contracts;
    using MorseCodeDecoder.Common.Encoder;

    /// <summary>
    /// Clase que inicializa las configuraciones para el servidor web.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">Objeto de configuración provisto por el framework.</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        /// <summary>
        /// Devuelve un objecto provisto por el framework con configuraciones varias.
        /// <para>Use este objecto para acceder a variables de entorno, configuraciones especificas por ambiente, etc.</para>
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Service collection.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Agrega dependencias necesarias para resolver el business.
            services.AddScoped<IMessageDecoder, MorseMessageManager>();
            services.AddScoped<IMessageEncoder, MorseMessageManager>();
            services.AddScoped<IMorseCodeDecoderBusiness, MorseCodeDecoderBusiness>();

            // Agrega las dependencias para Swagger.
            services.AddSwaggerGen();

            // Agrega las dependencias necesarias para el uso de controladores.
            services.AddControllers();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Application builder.</param>
        /// <param name="env">WebHost environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
