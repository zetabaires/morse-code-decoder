﻿namespace MorseCodeDecoder.Common
{
    /// <summary>
    /// Clase estatica con constantes.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Constante que representa el valor de un espacio ( ).
        /// </summary>
        public const char SPACE = ' ';

        /// <summary>
        /// Constante que representa el valor de un punto (.).
        /// </summary>
        public const char DOT = '.';

        /// <summary>
        /// Constante que representa el valor de un guion (-).
        /// </summary>
        public const char DASH = '-';

        /// <summary>
        /// Constante que representa el valor de un espacio vacio ().
        /// </summary>
        public const string EMPTY = "";
    }
}
