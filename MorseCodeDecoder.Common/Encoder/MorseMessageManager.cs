﻿namespace MorseCodeDecoder.Common.Encoder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using MorseCodeDecoder.Common.Contracts;

    /// <summary>
    /// Implementación de menejador para codificar y decodificar mensajes MORSE.
    /// </summary>
    public class MorseMessageManager : IMessageDecoder, IMessageEncoder
    {
        private readonly ILogger<MorseMessageManager> logger;
        private Dictionary<string, char> mapper = new Dictionary<string, char>()
        {
            { ".-", 'A' },
            { "-...", 'B' },
            { "-.-.", 'C' },
            { "-..", 'D' },
            { ".", 'E' },
            { "..-.", 'F' },
            { "--.", 'G' },
            { "....", 'H' },
            { "..", 'I' },
            { ".---", 'J' },
            { "-.-", 'K' },
            { ".-..", 'L' },
            { "--", 'M' },
            { "-.", 'N' },
            { "---", 'O' },
            { ".--.", 'P' },
            { "--.-", 'Q' },
            { ".-.", 'R' },
            { "...", 'S' },
            { "-", 'T' },
            { "..-", 'U' },
            { "...-", 'V' },
            { ".--", 'W' },
            { "-..-", 'X' },
            { "-.--", 'Y' },
            { "--..", 'Z' },
            { "-----", '0' },
            { ".----", '1' },
            { "..---", '2' },
            { "...--", '3' },
            { "....-", '4' },
            { ".....", '5' },
            { "-....", '6' },
            { "--...", '7' },
            { "---..", '8' },
            { "----.", '9' },
            { Constants.SPACE.ToString(), Constants.SPACE },
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="MorseMessageManager"/> class.
        /// </summary>
        /// <param name="logger">Instancia de logger para la clase actual.</param>
        public MorseMessageManager(ILogger<MorseMessageManager> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Método de instancia que realiza el decodificado de un mensaje en formato MORSE.
        /// </summary>
        /// <param name="message">Mensaje en formato MORSE.</param>
        /// <returns>Mensaje en formato Humano.</returns>
        public Task<string> DecodeMessage(string message)
        {
            this.logger.LogInformation("DecodeMessage: " + message);
            this.logger.LogDebug("Por validar el mensaje.");

            // Valida que el mensaje sea procesable.
            IList<string> errors;
            if (!this.Validate(message, out errors) || !this.ValidateDecoding(message, errors))
            {
                throw new ArgumentException("El mensaje no es correcto!. Errores: " + string.Join(Environment.NewLine, errors));
            }

            this.logger.LogDebug("Por realizar la conversión del mensaje.");

            // Busca por cada caracter de la cadena su equivalente en el diccionario.
            string result = string.Join(string.Empty, message
                .Split(Constants.SPACE)
                .Where(m => !string.IsNullOrEmpty(m))
                .Select(m => this.mapper[m]));

            this.logger.LogDebug("Mensaje transformado: " + result);

            return Task.FromResult(result);
        }

        /// <summary>
        /// Método de instancia que realiza el codificado de un mensaje a formato MORSE.
        /// </summary>
        /// <param name="message">Mensaje en formato Humano.</param>
        /// <returns>Mensaje en formato MORSE.</returns>
        public Task<string> EncodeMessage(string message)
        {
            this.logger.LogInformation("EncodeMessage: " + message);

            this.logger.LogDebug("Por normalizar el mensaje.");

            // Normaliza el mensaje para evitar conflictos entre mayusculas y minusculas.
            message = GetNormalizedMessage(message);

            this.logger.LogDebug("Por validar el mensaje.");

            // Valida que el mensaje sea procesable.
            IList<string> errors;
            if (!this.Validate(message, out errors) || !this.ValidateEncoding(message, errors))
            {
                throw new ArgumentException("El mensaje no es correcto!. Errores: " + string.Join(Environment.NewLine, errors));
            }

            this.logger.LogDebug("Por realizar la conversión del mensaje.");

            // Busca por cada caracter de la cadena su equivalente en el diccionario.
            string result = string.Join(Constants.SPACE, message.Select(m => this.mapper.First(x => x.Value == m).Key));

            this.logger.LogDebug("Mensaje transformado: " + result);

            return Task.FromResult(result);
        }

        /// <summary>
        /// Método estático que devuelve un mensaje normalizado para no tener conflictos a la hora de decodificar.
        /// </summary>
        /// <param name="message">Mensaje actual.</param>
        /// <returns>Mensaje normalizado en mayuscula.</returns>
        private static string GetNormalizedMessage(string message)
        {
            return message?.ToUpperInvariant();
        }

        /// <summary>
        /// Método de instancia que realiza validaciones necesarias.
        /// </summary>
        /// <param name="message">Mensaje actual.</param>
        /// <param name="errors">Listado de errores, si hubieran.</param>
        /// <returns>Estado de la validación. TRUE si el mensaje es válido, FALSE si no lo es.</returns>
        private bool Validate(string message, out IList<string> errors)
        {
            errors = new List<string>();

            if (string.IsNullOrEmpty(message))
            {
                errors.Add("El mensaje está vacio.");
            }

            this.logger.LogDebug("Validó que el mensaje no esté vacio.");

            return !errors.Any();
        }

        /// <summary>
        /// Método de instancia que realiza validaciones necesarias para la decodificación del mensaje.
        /// </summary>
        /// <param name="message">Mensaje actual que se quiere decodificar.</param>
        /// <param name="errors">Listado de errores, si hubieran.</param>
        /// <returns>Estado de la validación. TRUE si el mensaje es válido, FALSE si no lo es.</returns>
        private bool ValidateDecoding(string message, IList<string> errors)
        {
            char[] available = new char[] { Constants.DOT, Constants.DASH, Constants.SPACE };

            if (message?.Any(m => !available.Contains(m)) == true)
            {
                errors.Add("Hay caracteres inválidos en la secuencia.");
            }

            this.logger.LogDebug("Validó que el mensaje no contenga caracteres especiales.");

            return !errors.Any();
        }

        /// <summary>
        /// Método de instancia que realiza validaciones necesarias para la codificación del mensaje.
        /// </summary>
        /// <param name="message">Mensaje actual que se quiere codificar.</param>
        /// <param name="errors">Listado de errores, si hubieran.</param>
        /// <returns>Estado de la validación. TRUE si el mensaje es válido, FALSE si no lo es.</returns>
        private bool ValidateEncoding(string message, IList<string> errors)
        {
            // Valida que en el mensaje a encodear solo hayan caracteres conocidos y transformables 
            // (caracteres existentes en el diccionario)
            if (message?.Any(m => !this.mapper.Select(p => p.Value).Contains(m)) == true)
            {
                errors.Add("Hay caracteres inválidos en la secuencia.");
            }

            this.logger.LogDebug("Validó que el mensaje no contenga caracteres especiales.");

            return !errors.Any();
        }
    }
}
