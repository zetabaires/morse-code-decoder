﻿namespace MorseCodeDecoder.Common.Encoder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using MorseCodeDecoder.Common.Contracts;

    /// <summary>
    /// Implementación de IMessageDecoder para decodificar mensajes binarios.
    /// </summary>
    public class BinaryMessageDecoder : IMessageDecoder
    {
        private readonly ILogger<BinaryMessageDecoder> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="BinaryMessageDecoder"/> class.
        /// </summary>
        /// <param name="logger">Instancia de logger para la clase actual.</param>
        public BinaryMessageDecoder(ILogger<BinaryMessageDecoder> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Método de instancia que realiza el decodificado de un mensaje en formato binario.
        /// </summary>
        /// <param name="message">Mensaje en formato binario.</param>
        /// <returns>Mensaje en formato MORSE.</returns>
        public Task<string> DecodeMessage(string message)
        {
            this.logger.LogInformation("DecodeMessage: " + message);

            this.logger.LogDebug("Por validar el mensaje.");

            // Valida que el mensaje sea procesable.
            IList<string> errors;
            if (!this.Validate(message, out errors))
            {
                throw new ArgumentException("El mensaje no es correcto!. Errores: " + string.Join(Environment.NewLine, errors));
            }

            this.logger.LogDebug("Por realizar la conversión del mensaje.");

            // Separa los pulsos según por cambios de estado (entre pulsos y pausas)
            var result = Regex.Matches(message, "(0+|1+)");

            // Obtiene los promedios para identificar la duración
            int avaragePulse = (int)result.Where(m => m.Value.Contains("1")).Select(m => m.Value.Length).Average();
            int avaragePause = (int)result.Where(m => m.Value.Contains("0")).Select(m => m.Value.Length).Average();

            this.logger.LogDebug($"Promedios obtenidos. Pulsos: {avaragePulse}, Pausas: {avaragePause}");

            this.logger.LogDebug($"Por recorrer {result.Count} pulsos o pausas.");

            string newMessage = string.Empty;
            foreach (Match pulseOrPause in result)
            {
                // Obtiene el valor del pulso o la pausa actual
                string val = pulseOrPause.Value;

                // Mide el tamaño o duración actual
                int len = val.Length;

                newMessage += val.Contains("1")
                    // Si el valor contiene un "1" es un pulso
                    ? len <= avaragePulse
                        // Si el pulso es corto corresponde a un punto
                        ? Constants.DOT.ToString()
                        // Si el pulso es largo corresponde a un guion
                        : Constants.DASH.ToString()
                    // Si no contiene un "1" entonces es una pausa
                    : len <= avaragePause
                        // Si la pausa es corta no muestra cambios
                        ? Constants.EMPTY
                        // Si la pausa es larga corresponde a un espacio (cambio de letra)
                        : Constants.SPACE.ToString();
            }

            // Limpia espacios innecesarios
            newMessage = newMessage.Trim();

            this.logger.LogDebug("Mensaje transformado: " + newMessage);

            return Task.FromResult(newMessage);
        }

        /// <summary>
        /// Método de instancia que realiza validaciones necesarias para la decodificación del mensaje.
        /// </summary>
        /// <param name="message">Mensaje actual que se quiere decodificar.</param>
        /// <param name="errors">Listado de errores, si hubieran.</param>
        /// <returns>Estado de la validación. TRUE si el mensaje es válido, FALSE si no lo es.</returns>
        private bool Validate(string message, out IList<string> errors)
        {
            errors = new List<string>();

            if (string.IsNullOrEmpty(message))
            {
                errors.Add("El mensaje está vacio.");
            }

            this.logger.LogDebug("Validó que el mensaje no esté vacio.");

            char[] available = new char[] { '0', '1' };

            if (message?.Any(m => !available.Contains(m)) == true)
            {
                errors.Add("Hay caracteres inválidos en la secuencia.");
            }

            this.logger.LogDebug("Validó que el mensaje no contenga caracteres no binarios.");

            return !errors.Any();
        }
    }
}
