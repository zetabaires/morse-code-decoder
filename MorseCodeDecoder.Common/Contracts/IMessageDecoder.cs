﻿namespace MorseCodeDecoder.Common.Contracts
{
    using System.Threading.Tasks;

    /// <summary>
    /// Interfaz que determina el contrato para un decodificador.
    /// </summary>
    public interface IMessageDecoder
    {
        /// <summary>
        /// Método que realiza el decodificado de un mensaje.
        /// </summary>
        /// <param name="message">Mensaje que se intenta decodificar.</param>
        /// <returns>Resultado del mensaje decodificado.</returns>
        Task<string> DecodeMessage(string message);
    }
}
