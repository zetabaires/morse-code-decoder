﻿namespace MorseCodeDecoder.Common.Contracts
{
    using System.Threading.Tasks;

    /// <summary>
    /// Interfaz que determina el contrato para un codificador.
    /// </summary>
    public interface IMessageEncoder
    {
        /// <summary>
        /// Método que realiza el codificado de un mensaje.
        /// </summary>
        /// <param name="message">Mensaje que se intenta codificar.</param>
        /// <returns>Resultado del mensaje codificado.</returns>
        Task<string> EncodeMessage(string message);
    }
}
